;;; init.el --- Emacs init file                      -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Milan Jovanovic

;; Author: Milan Jovanovic;; Set default coding system <milan@lenovo>
;; Keywords: lisp, abbrev

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Emacs init file

;;; Code:

;; Set default encoding
(set-default-coding-systems 'utf-8)
;; (menu-bar-mode -1)
(tool-bar-mode -1)
;; Set up package.el to work with MELPA
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(package-refresh-contents)

;; Use package macro
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))
(require 'bind-key)

;; Exec path from shell
(use-package exec-path-from-shell
  :ensure t
  :config
  (exec-path-from-shell-initialize))

;; Evil
(use-package evil
  :ensure t
  :config
  (evil-mode 1))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(custom-enabled-themes (quote (monokai)))
 '(custom-safe-themes
   (quote
    ("a2cde79e4cc8dc9a03e7d9a42fabf8928720d420034b66aecc5b665bbf05d4e9" default)))
 '(helm-completion-style (quote emacs))
 '(package-selected-packages
   (quote
    (lsp-ui company-phpactor phpactor company-lsp racer racer-mode docker helm-gtags multi-term posframe dap-mode evil-surround ctags-update lsp-treemacs lsp-mode rustic php-auto-yasnippets php-refactor-mode yaml-mode phps-mode monokai-theme web-mode flycheck magit yasnippet-snippets fuzzy projectile helm-git dired-sidebar centaur-tabs evil)))
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Hack" :foundry "SRC" :slant normal :weight normal :height 98 :width normal)))))

;; Auto complete
;; (use-package auto-complete
;;   :after fuzzy
;;   :ensure t
;;   :config
;;   (ac-config-default)
;;   (setq ac-use-fuzzy 1))

;; Company complete
(use-package company
  :ensure t)
(global-set-key "\t" 'company-complete-common)
(define-key company-mode-map [remap indent-for-tab-command] #'company-indent-or-complete-common)

;; Company php
;; (use-package company-php
;;   :ensure t)

;; Helm
(use-package helm
  :ensure t
  :config
  (helm-mode 1)
  :bind
  ("M-x" . helm-M-x)
  ("C-x C-f" . helm-find-files))

;; Helm git
(use-package helm-ls-git
  :ensure t)

;; PHP mode
(use-package php-mode
  :ensure t
  :hook
  (php-mode . company-mode)
  ((php-mode . (lambda () (set (make-local-variable 'company-backends)
                               '((company-phpactor)
                                 company-capf company-files)
                               )))))
(add-hook 'php-mode-hook
          (lambda ()
            (make-local-variable 'eldoc-documentation-function)
            (setq eldoc-documentation-function
                  'phpactor-hover)))

;; Ac php
;; (use-package ac-php
;;   :ensure t
;;   :after php-mode)

;; Phpactor
(use-package phpactor
  :ensure t
  :after php-mode)

;; Company phpactor
(use-package company-phpactor
  :ensure t
  :after php-mode)

;; Company Lsp
(use-package company-lsp
  :ensure t
  :init
  (push 'company-lsp company-backends))

;; Racer
(use-package racer
  :ensure t
  :after rustic
  :hook ((rustic-mode . racer-mode)
         (racer-mode . eldoc-mode)
	 (racer-mode . company-mode)))
(setq company-tooltip-align-annotations t)

;; Auto complete php
;; Company hook
;; (add-hook 'php-mode-hook
;; 	  '(lambda ()
;; 	     ;; Enable company-mode
;; 	     (company-mode t)
;; 	     (require 'company-php)

;; 	     ;; Enable ElDoc support (optional)
;; 	     (ac-php-core-eldoc-setup)

;; 	     (set (make-local-variable 'company-backends)
;; 		  '((company-ac-php-backend company-dabbrev-code)
;; 		    company-lsp company-files))

;; 	     ;; Jump to definition (optional)
;; 	     (define-key php-mode-map (kbd "M-]")
;; 	       'ac-php-find-symbol-at-point)

;; 	     ;; Return back (optional)
;; 	     (define-key php-mode-map (kbd "M-[")
;; 	       'ac-php-location-stack-back)))
;; ;; Autocomplete hook
;; (add-hook 'php-mode-hook
;;           '(lambda ()
;;              ;; Enable auto-complete-mode
;;              (auto-complete-mode t)

;;              (require 'ac-php)
;;              (setq ac-sources '(ac-source-php))

;;              ;; As an example (optional)
;;              (yas-global-mode 1)

;;              ;; Enable ElDoc support (optional)
;;              (ac-php-core-eldoc-setup)

;;              ;; Jump to definition (optional)
;;              (define-key php-mode-map (kbd "M-]")
;;                'ac-php-find-symbol-at-point)

;;              ;; Return back (optional)
;;              (define-key php-mode-map (kbd "M-[")
;;                'ac-php-location-stack-back)))

;; Fuzzy
;; (use-package fuzzy
;;   :ensure t)

;; Ac emoji
;; (use-package ac-emoji
;;   :ensure t)

;; Yasnippets
(use-package yasnippet-snippets
  :ensure t)

;; Centaur tab
(use-package centaur-tabs
  :demand
  :init
  (setq centaur-tabs-style "bar")
  (setq centaur-tabs-height 32)
  (setq centaur-tabs-set-bar 'over)
  (add-hook 'after-init-hook 'centaur-tabs-headline-match)
  :config
  (centaur-tabs-mode t)
  :bind
  ("C-c g" . 'centaur-tabs-switch-group)
  ("C-<prior>" . centaur-tabs-backward)
  ("C-<next>" . centaur-tabs-forward))

;; Dired sidebar
(use-package dired-sidebar
  :bind (("C-c C-n" . dired-sidebar-toggle-sidebar))
  :ensure t
  :commands (dired-sidebar-toggle-sidebar))

;; Projectile
(use-package projectile
  :ensure t
  :config
  (projectile-mode +1)
  :bind-keymap
  ("C-c p" . projectile-command-map))

;; Magit
(use-package magit
  :ensure t
  :commands
  (magit-status)
  :bind
  ("C-x g" . magit-status))

;; Flycheck
(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

;; Web mode
(use-package web-mode
  :ensure t)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(setq web-mode-engines-alist
      '(("php"    . "\\.phtml\\'")
	("blade"  . "\\.blade\\."))
      )

;; Yaml mode
(use-package yaml-mode
  :ensure t)
(add-hook 'yaml-mode-hook
          (lambda ()
            (define-key yaml-mode-map "\C-m" 'newline-and-indent)))

;; Php auto yasnippets
(use-package php-auto-yasnippets
  :ensure t)

;; Rust mode
;; (use-package rust-mode
;;   :ensure t)

;; Rustic (Rust mode alternative)
(use-package rustic
  :ensure t)
(setq rustic-format-on-save t)

;; Lsp mode
(use-package lsp-mode
  :ensure t)
(use-package lsp-ui
  :ensure t
  :after lsp-mode)
(use-package lsp-treemacs
  :ensure t
  :after lsp-mode)
(setq lsp-ui-doc-position 'bottom)

;; Cargo
;; (use-package cargo
;;   :ensure t)
;; (add-hook 'rustic-mode-hook 'cargo-minor-mode)

;; Changing default temporary directory
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Ctags update
(use-package ctags-update
  :ensure t)

;; Avy bindings
(avy-setup-default)
(global-set-key (kbd "C-c C-j") 'avy-resume)

;; Evil Surround
(use-package evil-surround
  :ensure t
  :config
  (global-evil-surround-mode 1))

;; Dired default options
(setq dired-listing-switches "-lha --group-directories-first")
(autoload 'dired-async-mode "dired-async.el" nil t)
(dired-async-mode 1)

;; Dap mode
(use-package dap-mode
  :after posframe
  :ensure t
  :config
  (dap-ui-mode 1)
  (dap-tooltip-mode 1)
  (tooltip-mode 1))
(require 'dap-php)
(use-package posframe
  :ensure t)

;; Dap PHP debug
(dap-register-debug-template "Virtuelabs Debug"
                             (list :type "php"
                                   :cwd nil
                                   :request "launch"
                                   :name "Virtuelabs Debug"
                                   :pathMappings (ht ("/var/www/html" "/home/milan/Projects/Virtuelabs/www/magento2"))
                                   :sourceMaps t))

;; Set line numbers
(global-linum-mode)

;; Xml mode
(setq-default nxml-child-indent 4 nxml-attribute-indent 4)

;; Tabs to spaces
(setq-default indent-tabs-mode nil)

;; Multi term
(use-package multi-term
  :ensure t
  :config
  (setq multi-term-program "/bin/zsh"))


;; Helm gtags
(use-package helm-gtags
  :ensure t)

;; Docker mode
(use-package docker
  :ensure t
  :bind ("C-c d" . docker))

(provide 'init)
;;; init.el ends here
